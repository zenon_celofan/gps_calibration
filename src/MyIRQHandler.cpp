#include <stdio.h>
#include <stdlib.h>
#include "bluepill.h"
#include "IRQHandler.h"
#include "Led.h"
#include "Serial.h"
#include "IRQHandler.h"
#include "Rtc.h"
#include "Micros.h"


extern Led led_builtin;
extern uint32_t rtc_tick;
extern uint32_t gps_tick;


//nixie display refresh
void TIM2_IRQHandler(void) {
} //TIM2_IRQHandler


void EXTI0_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line0);
} //EXTI0_IRQHandler()


void EXTI1_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line1);
} //EXTI1_IRQHandler()


void EXTI2_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line2);
} //EXTI2_IRQHandler()


void EXTI3_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line3);
} //EXTI3_IRQHandler()


void EXTI4_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line4);
} //EXTI4_IRQHandler()


//touch buttons handler
void EXTI9_5_IRQHandler(void) {
} //EXTI9_5_IRQHandler()

void EXTI15_10_IRQHandler(void) {
	static uint32_t gtick;
	static uint32_t timestamp;

	EXTI_ClearITPendingBit(EXTI_Line10);
	gtick = micros();
	gps_tick = gtick - timestamp;
	timestamp = gtick;
} //EXTI15_10_IRQHandler()


void RTC_IRQHandler(void) {
	static uint32_t rtick;
	static uint32_t timestamp;

	RTC_ClearITPendingBit(RTC_IT_SEC);
	rtick = micros();
	rtc_tick = rtick - timestamp;
	timestamp = rtick;
} //RTC_IRQHandler();


void PVD_IRQHandler(void) {};
void TAMPER_IRQHandler(void) {};
void RTCAlarm_IRQHandler(void) {};
void USBWakeUp_IRQHandler(void) {};
