#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "bluepill.h"
#include "micros.h"
#include "Led.h"
#include "Rtc.h"
#include "Pin.h"
#include "Serial.h"
#include "timers.h"
#include "TouchButton.h"


Rtc rtc;
TouchButton	gps_pin(PA10, Bit_SET, ENABLE);
Led led_builtin(PC13, Bit_RESET);
Serial	debug_serial(USART3);

uint32_t rtc_tick;
uint32_t gps_tick;


void main() {

	char message[100];

	//init
	micros_init();
	debug_serial.send_it("\n\n\n*** gps_calibration - RESET ***\n\n ");
    led_builtin.turn_off();
	rtc.one_second_interrupt(ENABLE);

	while (1) {
		sprintf(message, "rtc tick: %lu\n", rtc_tick);
		debug_serial.send(message);

		sprintf(message, "gps tick: %lu\n\n", gps_tick);
		debug_serial.send(message);

		delay_us(500000);
	}
} //main()
